# coding: utf8
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import graphene_django_optimizer as gql_optimizer
import six
from django.db.models import Case
from django.db.models import When
from django.utils.functional import cached_property
from django_elasticsearch_dsl import Document
from django_elasticsearch_dsl.search import Search


class PatchedDocument(Document):
    @cached_property
    def _fields(self):
        return self._doc_type.mapping.properties.properties.to_dict()

    @classmethod
    def search(cls, using=None, index=None):
        return PatchedSearch(
            using=cls._get_using(using),
            index=cls._default_index(index),
            doc_type=[cls],
            model=cls.django.model
        )


class PatchedSearch(Search):
    MAX_ES_RESULTS = 10000

    def __init__(self, **kwargs):
        super(PatchedSearch, self).__init__(**kwargs)
        self._use_queryset_iter = False
        self._graphene_resolve_info = None

    def _clone(self) -> "PatchedSearch":
        s = super(PatchedSearch, self)._clone()
        s._use_queryset_iter = self._use_queryset_iter
        s._graphene_resolve_info = self._graphene_resolve_info
        return s

    all = _clone

    def use_queryset_iter(self, graphene_resolve_info=None):
        s = self._clone()
        s._use_queryset_iter = True
        s._graphene_resolve_info = graphene_resolve_info
        return s

    def __iter__(self):
        if self._use_queryset_iter:
            return iter(self.to_queryset())

        return super(PatchedSearch, self).__iter__()

    def count_real(self):
        if hasattr(self, "_response"):
            resp_total = self._response.hits.total
            if isinstance(resp_total, six.integer_types):
                return resp_total

            return resp_total.value

        return super(PatchedSearch, self).count()

    def count(self):
        real_count = self.count_real()
        if not self._use_queryset_iter:
            return real_count

        return min(real_count, self.MAX_ES_RESULTS)

    def to_queryset(self, keep_order=True):
        if hasattr(self, "_response"):
            result = self
        else:
            result = self.source(False).execute()

        pks = [hit.meta.id for hit in result]

        qs = self._model.objects.filter(pk__in=pks)

        if keep_order:
            preserved_order = Case(*iter(
                When(pk=pk, then=pos)
                    for pos, pk in enumerate(pks)
            ))
            qs = qs.order_by(preserved_order)

        return gql_optimizer.query(qs, self._graphene_resolve_info)
