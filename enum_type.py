# coding: utf8
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from collections import OrderedDict

import six


class EnumBaseMeta(type):
	__NON_VALUE_TYPES = (classmethod, staticmethod, dict, set, list,)

	def __new__(cls, clsname, bases, class_dict):
		cls = super(EnumBaseMeta, cls).__new__(cls, clsname, bases, class_dict)
		name_to_value_map = {}
		for c in reversed(cls.__bases__):
			if hasattr(c, "NAME_TO_VALUE_MAP"):
				name_to_value_map.update(c.NAME_TO_VALUE_MAP)

		if "MIN_VALUE" in class_dict:
			class_dict.pop("MIN_VALUE")
		if "MAX_VALUE" in class_dict:
			class_dict.pop("MAX_VALUE")

		name_order = class_dict.pop("NAME_ORDER", ())
		name_map = class_dict.pop("NAME_MAP", {})

		name_to_value_map.update(
			(n, v)
				for n, v in list(class_dict.items())
				if cls.__is_enum_item(n, v)
		)
		value_to_name_map = {v: k for k, v in name_to_value_map.items()}
		type.__setattr__(cls, "NAME_TO_VALUE_MAP", name_to_value_map)
		type.__setattr__(cls, "VALUE_TO_NAME_MAP", value_to_name_map)

		name_order = list(name_to_value_map.keys()) if not name_order else name_order
		type.__setattr__(cls, "NAME_ORDER", name_order)
		type.__setattr__(cls, "NAME_MAP", name_map)

		if name_to_value_map:
			type.__setattr__(cls, "MIN_VALUE", min(name_to_value_map.values()))
			type.__setattr__(cls, "MAX_VALUE", max(name_to_value_map.values()))

		return cls

	@classmethod
	def __is_enum_item(cls, key, value):
		if key.startswith("__"):
			return False

		if callable(value):
			return False

		if isinstance(value, cls.__NON_VALUE_TYPES):
			return False

		return True

	def __setattr__(cls, name, value):
		raise AttributeError("Class '{0}' is read-only.".format(cls.__name__))


@six.add_metaclass(EnumBaseMeta)
class EnumBase(object):
	"""
		usage:
			class ExampleType(EnumBase):
				NONE, SUCCESS, FAIL = range(3)
	"""
	NAME_TO_VALUE_MAP = {}
	VALUE_TO_NAME_MAP = {}
	MIN_VALUE = 0
	MAX_VALUE = 0
	NAME_ORDER = ()
	NAME_MAP = {}

	@classmethod
	def _format_name(cls, value, name):
		map_name = cls.NAME_MAP.get(value)
		return map_name if map_name is not None else name

	@classmethod
	def get_formatted_list(cls):
		formatted_list = list()
		for name in cls.NAME_ORDER:
			value = cls.get_value(name)
			formatted_name = cls._format_name(value, name)
			formatted_list.append(dict(name=formatted_name, value=value))

		return formatted_list

	@classmethod
	def get_formatted_name_to_value_map(cls):
		n2v_map = OrderedDict()

		for name in cls.NAME_ORDER:
			value = cls.get_value(name)
			formatted_name = cls._format_name(value, name)

			n2v_map[formatted_name] = value

		return n2v_map

	@classmethod
	def get_formatted_value_to_name_map(cls):
		n2v_map = OrderedDict()

		for name in cls.NAME_ORDER:
			value = cls.get_value(name)
			formatted_name = cls._format_name(value, name)

			n2v_map[value] = formatted_name

		return n2v_map

	get_formatted_n2v_map = get_formatted_name_to_value_map
	get_formatted_v2n_map = get_formatted_value_to_name_map

	@classmethod
	def get_value(cls, name):
		return cls.NAME_TO_VALUE_MAP.get(name)

	@classmethod
	def get_name(cls, value, do_format=False):
		name = cls.VALUE_TO_NAME_MAP.get(value)
		if do_format:
			name = cls._format_name(value, name)

		return name

	@classmethod
	def get_names(cls, do_format=False):
		names = list(cls.NAME_TO_VALUE_MAP.keys())
		if do_format:
			names = list(map(cls._format_name, names))

		return names

	@classmethod
	def get_values(cls):
		return list(cls.VALUE_TO_NAME_MAP.keys())
