# coding: utf8
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import hashlib
import inspect

import six

from .memoize import d__mem_memeoize


try:
	import config
except ImportError:
	config = None

try:
	from django.core.cache import caches
except ImportError:
	caches = None

FORCE_LOAD_KEY = "_force_load"

FUNC_INSPECT_CACHE = {}


@d__mem_memeoize
def gen_function_simple_unique_desc(func):
	func_name = func.__name__
	func_module = func.__module__
	func_line_no = func.__code__.co_firstlineno

	func_seed = "{m}.{f}+{l}".format(m=func_module, f=func_name, l=func_line_no)
	return func_seed


def generic_params_to_key(*args, **kwargs):
	params_str = six.text_type(args) + six.text_type(kwargs)
	return params_str.encode("utf8")


def empty_params_to_key(*args, **kwargs):
	return ""


def get_cls_full_name(class_):
	return "{0.__module__}.{0.__name__}".format(class_)


def get_obj_cls_full_name(object_):
	return get_cls_full_name(object_.__class__)


def class_params_to_key(cls, *args, **kwargs):
	full_class_name = get_cls_full_name(cls)
	return "{0}.{1}".format(full_class_name, generic_params_to_key(*args, **kwargs))


def _d__hash_params_to_key(func):
	def _wrap(*args, **kwargs):
		params_key = func(*args, **kwargs)
		return hashlib.sha256(six.ensure_binary(params_key)).hexdigest()

	return _wrap


def _generic_id_to_key(id_, *args, **kwargs):
	return "%s" % id_


def replace_param_in_tuple(tuple_, ids_position, ids):
	return tuple_[:ids_position] \
		   + (ids,) \
		   + tuple_[ids_position + 1:]


def is_func_has_force_load_arg(func):
	args_spec = inspect.getfullargspec(func)
	if FORCE_LOAD_KEY in args_spec.args:
		return True

	if args_spec.varargs and FORCE_LOAD_KEY in args_spec.varargs:
		return True

	if args_spec.varkw and FORCE_LOAD_KEY in args_spec.varkw:
		return True

	return False


def get_force_load_flag_from_kwargs(kwargs, func_has_force_load_arg=False):
	if func_has_force_load_arg:
		load_func = kwargs.get
	else:
		load_func = kwargs.pop

	return load_func(FORCE_LOAD_KEY, False)


def get_timeout_func(timeout):
	def _real_gen(timeout_):
		if callable(timeout_):
			return timeout_

		return lambda *args, **kwargs: timeout_

	timeout_func = _real_gen(timeout)

	if config.TEST and not config.DEBUG:
		return lambda *args, **kwargs: max(timeout_func(*args, **kwargs) / 100, 10)  # At least 10s

	return timeout_func


class memoize_cache(object):
	def __init__(self, timeout, key_prefix, params_to_key_func, cache_name="default"):
		self._key_prefix = key_prefix
		self._timeout_func = get_timeout_func(timeout)
		self._params_to_key_func = params_to_key_func
		self._cache = caches[cache_name]

	def _gen_cache_key(self, args, kwargs):
		params_key = self._params_to_key_func(*args, **kwargs)
		return "{prefix}:{key}".format(prefix=self._key_prefix, key=params_key)

	def __call__(self, func):
		func_has_force_load_arg = is_func_has_force_load_arg(func)

		def _wrap(*args, **kwargs):
			force_load = get_force_load_flag_from_kwargs(kwargs, func_has_force_load_arg)
			cache_key = self._gen_cache_key(args, kwargs)

			if not force_load:
				data = self._cache.get(cache_key)
				if data is not None:
					return data

			return self._real_call(func, cache_key, args, kwargs)

		return _wrap

	def _real_call(self, func, cache_key, args, kwargs):
		data = func(*args, **kwargs)

		data_timeout = self._timeout_func(*args, _result=data, **kwargs)
		if data_timeout > 0:
			self._cache.set(cache_key, data, timeout=data_timeout)

		return data


class SimpleMemoizeMixin(object):
	INIT_KEY_PREFIX = "__until_call__"

	def __init__(self, timeout=300, params_to_key_func=generic_params_to_key, *args, **kwargs):
		params_to_key_func = _d__hash_params_to_key(params_to_key_func)

		super(SimpleMemoizeMixin, self).__init__(
			timeout, self.INIT_KEY_PREFIX, params_to_key_func,
			*args, **kwargs
		)

	def __call__(self, func):
		self._key_prefix = self._gen_key_prefix_from_function(func)
		return super(SimpleMemoizeMixin, self).__call__(func)

	def _gen_key_prefix_from_function(self, func):
		func_key = gen_function_simple_unique_desc(func)
		return hashlib.md5(six.ensure_binary(func_key)).hexdigest()


class simple_memoize_cache(SimpleMemoizeMixin, memoize_cache):
	pass


class memoize_ids_cache(object):
	def __init__(
			self, timeout, key_prefix, params_to_key_func=_generic_id_to_key,
			ids_position=0, cache_name="default",
	):
		self._key_prefix = key_prefix
		self._timeout_func = get_timeout_func(timeout)
		self._params_to_key_func = params_to_key_func
		self._ids_position = ids_position
		self._cache = caches[cache_name]

	def _gen_cache_key(self, id_, args, kwargs):
		params_key = self._params_to_key_func(id_, *args, **kwargs)
		return "{prefix}:{key}".format(prefix=self._key_prefix, key=params_key)

	def __call__(self, func):
		func_has_force_load_arg = is_func_has_force_load_arg(func)

		def _wrap(*args, **kwargs):
			ids = args[self._ids_position]
			force_load = get_force_load_flag_from_kwargs(kwargs, func_has_force_load_arg)

			if not ids or force_load:
				return self._real_call(func, ids, args, kwargs)

			cache_keys = [
				self._gen_cache_key(id_, args, kwargs)
				for id_ in ids
			]

			cache_data_map = self._cache.get_many(cache_keys)

			result_data_map = {}
			need_load_ids = []
			need_load_cache_keys = []
			for id_, cache_key in zip(ids, cache_keys):
				cache_data = cache_data_map.get(cache_key)

				if cache_data is None:
					need_load_ids.append(id_)
					need_load_cache_keys.append(cache_key)
					continue

				result_data_map[id_] = cache_data

			if need_load_ids:
				load_data_map = self._real_call(
					func, need_load_ids, args, kwargs,
					cache_keys=need_load_cache_keys)
				result_data_map.update(load_data_map)

			return result_data_map

		return _wrap

	def _real_call(self, func, ids, args, kwargs, cache_keys=None):
		original_ids = args[self._ids_position]
		if len(ids) < len(original_ids):
			func_args = replace_param_in_tuple(args, self._ids_position, ids)
		else:
			func_args = args

		data_map = func(*func_args, **kwargs)
		if not ids:
			return data_map

		if not cache_keys:
			cache_keys = [
				self._gen_cache_key(id_, args, kwargs)
				for id_ in ids
			]

		cache_data_map = {
			cache_keys[i]: data_map.get(ids[i])
			for i in range(len(ids))
		}

		data_timeout = self._timeout_func(*args, **kwargs)
		if data_timeout > 0:
			self._cache.set_many(cache_data_map, timeout=data_timeout)

		return data_map


class simple_memoize_ids_cache(SimpleMemoizeMixin, memoize_ids_cache):
	pass


class invalidate_memoize_cache(object):
	def __init__(self, key_prefix, params_to_key_func, cache_name="default"):
		self._key_prefix = key_prefix
		self._params_to_key_func = params_to_key_func
		self._cache = caches[cache_name]

	def __call__(self, func):
		def _wrap(*args, **kwargs):
			cache_key = self._gen_cache_key(args, kwargs)
			self._cache.delete(cache_key)

			return func(*args, **kwargs)

		return _wrap

	def _gen_cache_key(self, args, kwargs):
		params_key = self._params_to_key_func(*args, **kwargs)
		return "{prefix}:{key}".format(prefix=self._key_prefix, key=params_key)
