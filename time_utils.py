# coding: utf8
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import calendar
import re
import time
from datetime import date
from datetime import datetime
from datetime import timedelta

import pytz
import six


class TimeUtils(object):
	DEFAULT_TIMEZONE = "Asia/Ho_Chi_Minh"

	try:
		from django.conf import settings
		TIMEZONE = getattr(settings, "TIME_ZONE", DEFAULT_TIMEZONE)
	except Exception:
		TIMEZONE = DEFAULT_TIMEZONE

	ONE_MONTH_AVERAGE_DAYS = 30
	ONE_WEEK_DAYS = 7
	ONE_DAY_HOURS = 24
	ONE_HOUR_MINUTES = 60

	ONE_MINUTE_SECONDS = int(timedelta(minutes=1).total_seconds())
	ONE_HOUR_SECONDS = ONE_MINUTE_SECONDS * ONE_HOUR_MINUTES
	ONE_DAY_SECONDS = ONE_HOUR_SECONDS * ONE_DAY_HOURS
	ONE_WEEK_SECONDS = ONE_DAY_SECONDS * ONE_WEEK_DAYS

	ONE_DAY_DELTA = timedelta(days=1)

	HOUR_MINUTE_FORMAT = "%H:%M"
	DATE_FORMAT = "%Y-%m-%d"
	DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
	DATETIME_WITHOUT_SECOND_FORMAT = "%Y-%m-%d %H:%M"

	@classmethod
	def get_tz(cls, country_code):
		try:
			return pytz.country_timezones(country_code)[0]
		except Exception:
			return cls.TIMEZONE

	@classmethod
	def get_pytz_timezone(cls, tz, country_code):
		if tz is None:
			tz = cls.TIMEZONE

		if country_code:
			tz = cls.get_tz(country_code)

		return pytz.timezone(tz)

	@classmethod
	def to_ts(cls, dynamic_input, tz=None, country_code=None):
		if isinstance(dynamic_input, date):
			return cls.date_to_ts(dynamic_input, tz=tz, country_code=country_code)

		elif isinstance(dynamic_input, datetime):
			return cls.datetime_to_ts(dynamic_input, tz=tz, country_code=country_code)

		elif isinstance(dynamic_input, six.integer_types):
			return dynamic_input

		raise ValueError("Cannot parse type %s as time input." % dynamic_input.__class__)

	@classmethod
	def datetime_to_ts(cls, dt, tz=None, country_code=None):
		if dt and dt.tzinfo is None:
			tz = cls.get_pytz_timezone(tz, country_code)
			dt = tz.localize(dt)

		return calendar.timegm(dt.utctimetuple()) if dt else None

	@classmethod
	def datetime_to_str(cls, dt, format_=DATE_FORMAT, tz=None, country_code=None):
		tz = cls.get_pytz_timezone(tz, country_code)
		if dt and dt.tzinfo is None:
			dt = tz.localize(dt)

		return dt.strftime(format_)

	@classmethod
	def ts_to_datetime(cls, timestamp, tz=None, country_code=None):
		utc_dt = datetime.utcfromtimestamp(timestamp).replace(tzinfo=pytz.utc)
		tz = cls.get_pytz_timezone(tz, country_code)
		return tz.normalize(utc_dt.astimezone(tz))

	@classmethod
	def ts_to_str(cls, timestamp, format_=DATE_FORMAT, tz=None, country_code=None):
		dt_tz = cls.ts_to_datetime(timestamp, tz, country_code)
		return dt_tz.strftime(format_)

	@classmethod
	def ts_to_time_str(cls, timestamp, tz=None, country_code=None):
		return cls.ts_to_str(timestamp, cls.DATETIME_FORMAT, tz, country_code)

	@classmethod
	def str_to_datetime(cls, date_str, format_=DATE_FORMAT, tz=None, country_code=None):
		tz = cls.get_pytz_timezone(tz, country_code)
		return tz.localize(datetime.strptime(date_str, format_))

	@classmethod
	def str_to_ts(cls, date_str, format_=DATE_FORMAT, tz=None, country_code=None):
		dt = cls.str_to_datetime(date_str, format_, tz, country_code)
		return calendar.timegm(dt.utctimetuple()) if dt else None

	@classmethod
	def date_to_datetime(cls, date_, tz=None, country_code=None):
		utc_ts = calendar.timegm(date_.timetuple())
		utc_dt = datetime.utcfromtimestamp(utc_ts)

		tz = cls.get_pytz_timezone(tz, country_code)
		return tz.localize(utc_dt)

	@classmethod
	def date_to_ts(cls, date_, tz=None, country_code=None):
		dt_tz = cls.date_to_datetime(date_, tz, country_code)
		return cls.datetime_to_ts(dt_tz, tz, country_code)

	@classmethod
	def date_to_str(cls, date_, format_=DATE_FORMAT, tz=None, country_code=None):
		dt_tz = cls.date_to_datetime(date_, tz, country_code)
		return cls.datetime_to_str(dt_tz, format_, tz, country_code)

	@classmethod
	def set_datetime_tz(cls, dt, tz=None, country_code=None):
		tz = cls.get_pytz_timezone(tz, country_code)
		return dt.astimezone(tz)

	@classmethod
	def now(cls, in_real=False):
		now = time.time()
		if not in_real:
			now = int(now)

		return now

	@classmethod
	def today(cls, tz=None, country_code=None):
		utc_dt = datetime.utcnow().replace(tzinfo=pytz.utc)
		tz = cls.get_pytz_timezone(tz, country_code)

		return tz.normalize(utc_dt.astimezone(tz))

	@classmethod
	def today_str(cls, format_=DATE_FORMAT, tz=None, country_code=None):
		dt_tz = cls.today(tz=tz, country_code=country_code)
		return dt_tz.strftime(format_)

	@classmethod
	def round_datetime(cls, datetime_obj, tz=None, country_code=None):
		tz = cls.get_pytz_timezone(tz, country_code)
		dt_tz = tz.normalize(datetime_obj)
		dt_tz = dt_tz.replace(hour=0, minute=0, second=0, microsecond=0)

		return dt_tz

	@classmethod
	def round_ts(cls, timestamp=None, tz=None, country_code=None):
		if timestamp is None:
			timestamp = cls.now()

		utc_dt = datetime.utcfromtimestamp(timestamp).replace(tzinfo=pytz.utc)
		dt_tz = cls.round_datetime(utc_dt, tz, country_code)
		return calendar.timegm(dt_tz.utctimetuple())

	# http://stackoverflow.com/questions/4628122/how-to-construct-a-timedelta-object-from-a-simple-string#answer-4628148
	_TIME_DELTA_STR_REGEX = re.compile(
		r"[ ]*"
		r"((?P<days>\d+?)d)?[ ]*"
		r"((?P<hours>\d+?)h)?[ ]*"
		r"((?P<minutes>\d+?)m)?[ ]*"
		r"((?P<seconds>\d+?)s)?"
		r"[ ]*"
	)

	@classmethod
	def parse_timedelta(cls, time_delta_str, to_int=False):
		"""
		Convert from `timedelta` string to seconds.

		Examples:
		- "1d12h30m" => timedelta(days=1, hours=12, minutes=30)
		- "2h15m30s" => timedelta(hours=2, minutes=15, seconds=30)

		:param time_delta_str: Support these format_s:
		- d: days
		- h: hours
		- m: minutes
		- s: seconds
		"""
		parts = cls._TIME_DELTA_STR_REGEX.match(time_delta_str)
		if not parts:
			return timedelta()

		parts = parts.groupdict()
		time_params = {}
		for (name, param) in list(parts.items()):
			if param:
				time_params[name] = int(param)

		delta = timedelta(**time_params)
		if to_int:
			return int(delta.total_seconds())

		return delta
