# coding: utf8
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import datetime
import decimal
import uuid

import six


try:
	import simplejson as json
except Exception:
	import json


def _encoder_default(obj):
	# For Date Time string spec, see ECMA 262
	# https://ecma-international.org/ecma-262/5.1/#sec-15.9.1.15
	if isinstance(obj, datetime.datetime):
		representation = obj.isoformat()
		if representation.endswith("+00:00"):
			representation = representation[:-6] + "Z"

		return representation

	if isinstance(obj, datetime.date):
		return obj.isoformat()

	if isinstance(obj, datetime.timedelta):
		return six.text_type(obj.total_seconds())

	if isinstance(obj, decimal.Decimal):
		return float(obj)

	if isinstance(obj, uuid.UUID):
		return six.text_type(obj)

	if isinstance(obj, six.binary_type):
		return obj.decode("utf8")

	if hasattr(obj, "__getitem__"):
		try:
			return dict(obj)
		except Exception:
			pass

	if hasattr(obj, "__iter__"):
		return tuple(item for item in obj)

	raise TypeError(f"Object of type {obj.__class__.__name__} is not JSON serializable")


def to_json(data, ensure_bytes=False, separators=(",", ":"), default=_encoder_default, **kwargs):
	result = json.dumps(data, separators=separators, default=default, **kwargs)

	if ensure_bytes and isinstance(result, six.text_type):
		result = result.encode("utf-8")

	return result


def from_json(s):
	return json.loads(s)


def from_json_safe(s):
	try:
		return json.loads(s)
	except Exception:
		return None
