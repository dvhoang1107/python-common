# coding: utf8
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals


class d__mem_memeoize(dict):
	def __init__(self, func):
		super(d__mem_memeoize, self).__init__()
		self._func = func

	def __call__(self, *args, **kwargs):
		assert not kwargs, "mem_memeoize does not support kwargs."
		return self[args]

	def __missing__(self, args):
		data = self[args] = self._func(*args)
		return data
