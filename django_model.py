# coding: utf8
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import calendar
import datetime
import time
from fractions import Fraction
from typing import Dict
from typing import Type

import pytz
import six
from django import forms
from django.conf import settings
from django.contrib.admin import widgets
from django.contrib.postgres import lookups
from django.contrib.postgres.fields import JSONField
from django.core import exceptions
from django.core.exceptions import ValidationError
from django.core.validators import MaxLengthValidator
from django.db import models
from django.utils.deconstruct import deconstructible
from django.utils.translation import ugettext_lazy as _


def model_to_dict(obj, fields=None, exclude=None, ignore_null=False):
	data = {}
	for f in obj._meta.fields:
		if fields and f.name not in fields:
			continue
		elif exclude and f.name in exclude:
			continue
		elif ignore_null and getattr(obj, f.name, None) is None:
			continue

		data[f.name] = getattr(obj, f.name)

	return data


def model_from_dict(model, dict_data, ignore_null=False, force_inject=False):
	for key, value in dict_data.items():
		if not hasattr(model, key) and not force_inject:
			continue

		if ignore_null and value is None:
			continue

		setattr(model, key, value)

	return model


class DatabaseRouter(object):
	def db_for_read(self, model, instance=None, **hints):
		if hasattr(model, "Config"):
			if hasattr(model.Config, "db_for_read"):
				return model.Config.db_for_read

			if hasattr(model.Config, "db_for_all"):
				return model.Config.db_for_all

		if hasattr(settings, "DATABASE_APPS_MAPPING") \
				and model._meta.app_label in settings.DATABASE_APPS_MAPPING:

			mapping = settings.DATABASE_APPS_MAPPING[model._meta.app_label]
			if isinstance(mapping, Dict) and "read" in mapping:
				return mapping['read']

			return mapping

		return "default"

	def db_for_write(self, model, instance=None, **hints):
		if hasattr(model, "Config"):
			if hasattr(model.Config, "db_for_write"):
				return model.Config.db_for_write

			if hasattr(model.Config, "db_for_all"):
				return model.Config.db_for_all

		if hasattr(settings, "DATABASE_APPS_MAPPING") \
				and model._meta.app_label in settings.DATABASE_APPS_MAPPING:

			mapping = settings.DATABASE_APPS_MAPPING[model._meta.app_label]
			if isinstance(mapping, Dict) and "write" in mapping:
				return mapping['write']

			return mapping

		return "default"


class WidgetAdminSplitDateTimeTimestamp(widgets.AdminSplitDateTime):
	def decompress(self, value):
		if isinstance(value, six.integer_types):
			value = datetime.datetime.utcfromtimestamp(value).replace(tzinfo=pytz.utc)

		return super(WidgetAdminSplitDateTimeTimestamp, self).decompress(value)


class TimestampIntegerField(models.IntegerField):
	def __init__(self, verbose_name=None, name=None, auto_now=False, auto_now_add=False, **kwargs):
		self.auto_now, self.auto_now_add = auto_now, auto_now_add
		if auto_now or auto_now_add:
			kwargs['editable'] = False
			kwargs['blank'] = True

		super(TimestampIntegerField, self).__init__(verbose_name, name, **kwargs)

	def deconstruct(self):
		name, path, args, kwargs = super(TimestampIntegerField, self).deconstruct()
		if self.auto_now:
			kwargs['auto_now'] = True
		if self.auto_now_add:
			kwargs['auto_now_add'] = True
		if self.auto_now or self.auto_now_add:
			del kwargs['editable']
			del kwargs['blank']
		return name, path, args, kwargs

	def get_prep_value(self, value):
		if isinstance(value, datetime.datetime):
			value = calendar.timegm(value.utctimetuple())

		elif isinstance(value, datetime.date):
			value = calendar.timegm(value.timetuple())

		return super(TimestampIntegerField, self).get_prep_value(value)

	def pre_save(self, model_instance, add):
		if self.auto_now or (self.auto_now_add and add):
			value = int(time.time())
			setattr(model_instance, self.attname, value)
			return value

		return super(TimestampIntegerField, self).pre_save(model_instance, add)

	def to_python(self, value):
		if value is None:
			return value

		if isinstance(value, six.integer_types):
			return value

		if isinstance(value, datetime.datetime):
			return calendar.timegm(value.utctimetuple())

		if isinstance(value, datetime.date):
			return calendar.timegm(value.timetuple())

		raise ValidationError(
			self.error_messages['invalid'],
			code='invalid',
			params={'value': value},
		)

	def formfield(self, **kwargs):
		kwargs.update({
			'form_class': forms.SplitDateTimeField,
			'widget': WidgetAdminSplitDateTimeTimestamp,
		})
		return super(models.IntegerField, self).formfield(**kwargs)


class TimestampBigIntegerField(TimestampIntegerField, models.BigIntegerField):
	pass


@deconstructible
class MaxStrLengthValidator(MaxLengthValidator):
	code = 'max_str_length'

	def clean(self, x):
		return len(str(x))


class FractionCharField(models.CharField):
	default_error_messages = {
		'invalid': _("'%(value)s' value must be a faction."),
	}

	# noinspection PyMissingConstructor
	def __init__(self, *args, **kwargs):
		super(models.CharField, self).__init__(*args, **kwargs)
		self.validators.append(MaxStrLengthValidator(self.max_length))

		self._max_denominator = 10 ** (self.max_length / 2)

	def from_db_value(self, value, expression, connection, context):
		if value is None:
			return value

		return Fraction(value)

	def get_prep_value(self, value):
		if value is None:
			return None

		if isinstance(value, Fraction):
			value = value.limit_denominator(self._max_denominator)

		return six.text_type(value)

	def to_python(self, value):
		if value is None:
			return value

		try:
			return Fraction(value)
		except (TypeError, ValueError):
			raise ValidationError(
				self.error_messages['invalid'],
				code='invalid',
				params={'value': value},
			)


def d__register_json_field_lookups(class_: Type[JSONField]):
	for lookup in [
		lookups.DataContains,
		lookups.ContainedBy,
		lookups.HasKey,
		lookups.HasKeys,
		lookups.HasAnyKeys,
		lookups.JSONExact,
	]:
		class_.register_lookup(lookup)

	return class_


@d__register_json_field_lookups
class FractionJsonField(JSONField):
	default_error_messages = {
		'invalid': _("Value must be a valid number or Fraction."),
	}

	def __init__(self, *args, **kwargs):
		max_denominator_length = kwargs.pop("max_denominator_length", 32)
		self._max_denominator = 10 ** (max_denominator_length / 2)

		super(FractionJsonField, self).__init__(*args, **kwargs)

	def from_db_value(self, value, *args, **kwargs):
		if value is None:
			return value

		return Fraction(value['numerator'], value['denominator'])

	def get_prep_value(self, value):
		if value is None:
			return None

		if not isinstance(value, Fraction):
			value = Fraction(value)

		limited_value = value.limit_denominator(self._max_denominator)
		value_data = {
			'numerator': limited_value.numerator,
			'denominator': limited_value.denominator,
			'value': limited_value.numerator / limited_value.denominator,
		}

		return super(FractionJsonField, self).get_prep_value(value_data)

	def validate(self, value, model_instance):
		super(JSONField, self).validate(value, model_instance)
		try:
			Fraction(value)
		except TypeError:
			raise exceptions.ValidationError(
				self.error_messages['invalid'],
				code='invalid',
				params={'value': value},
			)

	def to_python(self, value):
		if value is None:
			return value

		if isinstance(value, Fraction):
			return value

		try:
			return Fraction(value)
		except (TypeError, ValueError):
			raise ValidationError(
				self.error_messages['invalid'],
				code='invalid',
				params={'value': value},
			)
