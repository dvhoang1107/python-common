# coding: utf8
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import re

import six


CHARACTER_CONVERSION_REGEX_TABLE = {
	'VN': {
		'[àáảãạăắằẵặẳâầấậẫẩ]': "a",
		'[đ]': "d",
		'[èéẻẽẹêềếểễệ]': "e",
		'[ìíỉĩị]': "i",
		'[òóỏõọôồốổỗộơờớởỡợ]': "o",
		'[ùúủũụưừứửữự]': "u",
		'[ỳýỷỹỵ]': "y",
		'[\u0301\u0300\u0309\u0303\u0323]': "",  # Accent marks in TELEX
	},
}


def region_string_to_ascii(text, region):
	mapping_table = CHARACTER_CONVERSION_REGEX_TABLE.get(region)
	if not mapping_table:
		return text

	output = six.ensure_text(text)
	for regex, replace in mapping_table.items():
		output = re.sub(regex, replace, output)
		# deal with upper case
		output = re.sub(regex.upper(), replace.upper(), output)

	return output
