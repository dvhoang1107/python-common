# coding: utf8
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from . import IS_DJANGO_APP


MIXED_LOGGER = None


def _log_record_exception(func):
	def _func(self):
		try:
			return func(self)
		except Exception:
			MIXED_LOGGER.exception(
				"log_exception|thread=%s:%s,file=%s:%s,func=%s:%s,log=%s",
				self.process, self.thread, self.filename, self.lineno,
				self.module, self.funcName, self.msg)
			raise

	return _func


def append_exc(func):
	def _append_exc(*args, **kwargs):
		if "exc_info" not in kwargs:
			kwargs['exc_info'] = True
		return func(*args, **kwargs)

	return _append_exc


def init_logger(log_dir=None, rollover_when="MIDNIGHT", rollover_backup_count=30):
	if log_dir is None:
		log_dir = "./log"

	import os
	import sys

	if log_dir != "@stdout":
		log_dir = os.path.abspath(log_dir)
		if log_dir and not os.path.exists(log_dir):
			os.mkdir(log_dir)

	if IS_DJANGO_APP:
		from django.conf import settings
		is_debug = settings.DEBUG
		is_test = "TEST" in dir(settings) and settings.TEST
	else:
		import config
		is_debug = "DEBUG" in dir(config) and config.DEBUG
		is_test = "TEST" in dir(config) and config.TEST

	formatter_map = {
		'standard': {
			'format': "%(asctime)s|%(levelname)s|%(process)d:%(thread)d|%(filename)s:%(lineno)d|%(module)s.%(funcName)s|%(message)s",
		},
		'short': {
			'format': "%(asctime)s|%(levelname)s|%(message)s",
		},
		'data': {
			'format': "%(asctime)s|%(message)s",
		},
	}
	logger_map = {
		'main': {
			'handlers': ["file_fatal", "file_error", "file_info", ],
			'level': "DEBUG",
			'propagate': True,
		},
		'data': {
			'handlers': ["file_data", ],
			'level': "DEBUG",
			'propagate': True,
		},
		'django.request': {
			'handlers': ["file_fatal", "file_error", "file_info", ],
			'level': "ERROR",
			'propagate': True,
		},
		'tornado.access': {
			'handlers': ["file_data", ],
			'level': "DEBUG",
			'propagate': True,
		},
		'tornado.application': {
			'handlers': ["file_fatal", "file_error", "file_info", ],
			'level': "DEBUG",
			'propagate': True,
		},
		'tornado.general': {
			'handlers': ["file_fatal", "file_error", "file_info", ],
			'level': "DEBUG",
			'propagate': True,
		},
	}
	handler_map = {
		'file_fatal': {
			'level': "CRITICAL",
			'class': "logging.handlers.TimedRotatingFileHandler",
			'filename': os.path.normpath(os.path.join(log_dir, "fatal.log")),
			'when': rollover_when,
			'backupCount': rollover_backup_count,
			'formatter': "standard",
		},
		'file_error': {
			'level': "WARNING",
			'class': "logging.handlers.TimedRotatingFileHandler",
			'filename': os.path.normpath(os.path.join(log_dir, "error.log")),
			'when': rollover_when,
			'backupCount': rollover_backup_count,
			'formatter': "standard",
		},
		'file_info': {
			'level': "DEBUG",
			'class': "logging.handlers.TimedRotatingFileHandler",
			'filename': os.path.normpath(os.path.join(log_dir, "info.log")),
			'when': rollover_when,
			'backupCount': rollover_backup_count,
			'formatter': "short",
		},
		'file_data': {
			'level': "DEBUG",
			'class': "logging.handlers.TimedRotatingFileHandler",
			'filename': os.path.normpath(os.path.join(log_dir, "data.log")),
			'when': rollover_when,
			'backupCount': rollover_backup_count,
			'formatter': "data",
		},
	}

	logger_config = {
		'version': 1,
		'disable_existing_loggers': True,
		'root': {
			'handlers': ["file_fatal", "file_error", "file_info", ],
			'level': "DEBUG" if is_test or is_debug else "INFO",
			'propagate': True,
		},
		'formatters': formatter_map,
		'handlers': handler_map,
		'loggers': logger_map,
	}

	if is_debug:
		handler_map['file_debug'] = {
			'level': "DEBUG",
			'class': "logging.handlers.TimedRotatingFileHandler",
			'filename': os.path.normpath(os.path.join(log_dir, "debug.log")),
			'when': rollover_when,
			'backupCount': rollover_backup_count,
			'formatter': "short",
		}
		logger_map["django.db.backends"] = {
			'handlers': ['file_debug'],
			'level': "DEBUG",
			'propagate': True,
		}

	elif not is_test:
		for name, config_ in logger_map.items():
			if config_['level'] == "DEBUG":
				config_['level'] = "INFO"

	if log_dir == "@stdout":
		logger_config['handlers'] = {
			'console': {
				'class': "logging.StreamHandler",
				'level': "DEBUG",
				'formatter': "short",
			}
		}
		for name, config_ in logger_map.items():
			config_['handlers'] = ["console", ]

	work_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), "../")
	recover_path = False
	if work_dir not in sys.path:
		sys.path.append(work_dir)
		recover_path = True

	import logging
	try:
		import logging.config
		logging.config.dictConfig(logger_config)
	except Exception as exc:
		import loggerconfig
		loggerconfig.dictConfig(logger_config)

	if recover_path:
		sys.path.remove(work_dir)

	global MIXED_LOGGER
	MIXED_LOGGER = logging.getLogger("main")
	MIXED_LOGGER.exception = append_exc(MIXED_LOGGER.error)
	MIXED_LOGGER.assertion = MIXED_LOGGER.critical
	MIXED_LOGGER.data = logging.getLogger("data").info
	logging.LogRecord.getMessage = _log_record_exception(logging.LogRecord.getMessage)


# try init log
def try_init_logger():
	try:
		from django.conf import settings

		setting_keys = dir(settings)
		if "LOGGER_CONFIG" in setting_keys:
			init_logger(**settings.LOGGER_CONFIG)
		elif "LOGGING" in setting_keys and settings.LOGGING:
			import logging
			global MIXED_LOGGER
			MIXED_LOGGER = logging.getLogger("main")
			MIXED_LOGGER.exception = append_exc(MIXED_LOGGER.error)
			MIXED_LOGGER.data = logging.getLogger("data").info
		else:
			init_logger()

	except Exception as exc_settings:
		try:
			import config
			init_logger(**config.LOGGER_CONFIG)
		except Exception as exc_config:
			try:
				init_logger()
			except Exception as exc_empty:
				pass


if MIXED_LOGGER is None:
	try_init_logger()

	debug = getattr(MIXED_LOGGER, "debug", None)
	info = getattr(MIXED_LOGGER, "info", None)
	data = getattr(MIXED_LOGGER, "data", None)
	warn = warning = getattr(MIXED_LOGGER, "warning", None)
	error = getattr(MIXED_LOGGER, "error", None)
	exception = getattr(MIXED_LOGGER, "exception", None)
	fatal = critical = getattr(MIXED_LOGGER, "critical", None)
