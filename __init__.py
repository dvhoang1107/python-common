# formatter:off
try:
	from django.conf import settings
	IS_DJANGO_APP = settings.configured

except ImportError:
	IS_DJANGO_APP = False
# formatter:on
